from django.conf.urls import url
from . import views

app_name = 'blog'
urlpatterns = [
	url(r'^$', views.index, name='index'),
	url(r'^(?P<article_id>[0-9]+)/$', views.article_detail, name='article_detail'),
	url(r'^categories/$', views.categories, name='categories'),
	url(r'^category/(?P<category_name>[a-zA-Z]+)/$', views.category_detail, name='category_detail'),
]