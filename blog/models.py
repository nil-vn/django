from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Category(models.Model):
	name = models.CharField(max_length=255)
	slug = models.CharField(max_length=255)
	def __str__(self):
		return self.name
	def the_name(self):
		return self.name
	def articles(self):
		articles = Article.objects.filter(category_id=self.id)
		return articles
class Article(models.Model):
	title = models.CharField(max_length=255)
	category = models.ForeignKey(Category, on_delete=models.CASCADE)
	content = models.TextField()
	created = models.DateTimeField(auto_now=False, auto_now_add=True)

	def __str__(self):
		return self.title
	def the_title(self):
		return self.title
	def the_content(self):
		return self.content
	def the_category(self):
		return self.category
	def created_at(self):
		return self.created