from django.shortcuts import render
from django.http import HttpResponse
from django.template import RequestContext, loader
from .models import Article, Category
# Create your views here.
def index(request):
	#return HttpResponse("hello, It's me")
	articles = Article.objects.all().order_by('category_id')
	return HttpResponse(loader.get_template('blog/index.html').render({'articless': articles}))

def article_detail(request, article_id):
	article 			= Article.objects.get(pk=article_id)
	related_articles 	= Article.objects.filter(category_id=article.category.id)
	template 		= loader.get_template('blog/detail.html')
	context 		= RequestContext(request, {
		'article': article,
		'related_articles': related_articles,
	})
	return HttpResponse(template.render(context))

def categories(request):
	categories = Category.objects.all()
	return HttpResponse(loader.get_template('blog/categories.html').render({'categories': categories}));
def category_detail(request, category_name):
	if category_name:
		category = Category.objects.get(slug=category_name)
		if category:
			result = {
				'status': 200,
				'data': category,
			}
		else:
			result = {
				'status' : 404,
				'message': 'Not found category \"%s\"' %category_name
			}
	return HttpResponse(loader.get_template('blog/category_detail.html').render(result));